#define D0 16
#define D1 5
#define D2 4
#define D3 0
#define D4 2
#define D5 14
#define D6 12
#define D7 13
#define D8 15

#define INITIAL_DEGREE 90
#define MAXIMUMN_RANGE 200
#define sensor D1
#define SETUP_AP_NAME "[SWiTH] Please setup to online."

const char *IO_SERVER = "api.swith.online";
const int IO_PORT = 80;

#include <SocketIoClient.h>
#include <Servo.h>
#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <String.h>
#include <ArduinoUniqueID.h>
#include <ArduinoJson.h>
#include <Timer.h>

// input pin
int SW_1 = D5;
int SW_2 = D4;
int SW_3 = D3;
const int echoPin = D1;
const int trigPin = D2;
// output pin
Servo servo1, servo2, servo3;
// libs.
Timer timer;
SocketIoClient socket;
// STATE
int SOCKET_IO_CONNECTED = 0;
String Board_UID;

int STATE_SERVO_1 = 0, STATE_SERVO_2 = 0, STATE_SERVO_3 = 0,
    TRIGGERED_RANGE = 0;

void setup()
{
  pinMode(SW_1, INPUT_PULLUP);
  pinMode(SW_3, INPUT_PULLUP);
  pinMode(SW_2, INPUT_PULLUP);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

  Serial.begin(115200);

  // WiFi.begin(ssid, password);
  UniqueIDdump(Serial);
  for (size_t i = 0; i < 8; i++)
  {
    if (UniqueID[i] < 0x10)
      Board_UID += "0";
    Board_UID += String(UniqueID[i]);
  }

  WiFiManager wifiManager;
  wifiManager.autoConnect(SETUP_AP_NAME);
  Serial.println("[SWiTH] Connecting WI-FI ");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(250);
    Serial.print(".");
  }
  Serial.println("[SWiTH] WiFi connected!");

  servo1.attach(D6);
  servo2.attach(D7);
  servo3.attach(D8);

  servoReset();

  socket.on("connect", socketConnected);
  socket.on("disconnected", on_disconnect);

  socket.on("control", handleServoControl);
  socket.on("update", handleUpdateServoState);

  socket.begin(IO_SERVER, IO_PORT);
  Serial.println("[SWiTH] Connecting to socket-io server ");
  timer.every(1000, handleSensor);
}

void loop()
{

  if (WiFi.status() == WL_CONNECTED)
  {
    timer.update();
    handlePushButtonSwitch();
    // handleSensor();
    if (SOCKET_IO_CONNECTED == 0)
    {
      delay(200);
      Serial.println("Retrying ....");
    }
    socket.loop();
  }
}

int latest_state_btn_1 = 0, latest_state_btn_2 = 1, latest_state_btn_3 = 1;

void handlePushButtonSwitch()
{

  if (digitalRead(SW_1) != latest_state_btn_1)
  {
    if (digitalRead(SW_1) == LOW)
    {
      String body = "{ \"board_id\": \"" + Board_UID + "\", \"button_no\": \"1\" }";
      char copy[255];
      body.toCharArray(copy, 255);
      socket.emit("push_button", copy);
      if (STATE_SERVO_1 == 1)
      {
        servoDown(1);
      }
      else
      {
        servoUp(1);
      }
    }
    delay(100);
  }
  latest_state_btn_1 = digitalRead(SW_1);

  if (digitalRead(SW_2) != latest_state_btn_2)
  {
    if (digitalRead(SW_2) == LOW)
    {
      String body = "{ \"board_id\": \"" + Board_UID + "\", \"button_no\": \"2\" }";
      char copy[255];
      body.toCharArray(copy, 255);
      socket.emit("push_button", copy);
      if (STATE_SERVO_2 == 1)
      {
        servoDown(2);
      }
      else
      {
        servoUp(2);
      }
    }
    delay(100);
  }
  latest_state_btn_2 = digitalRead(SW_2);
  if (digitalRead(SW_3) != latest_state_btn_3)
  {
    if (digitalRead(SW_3) == LOW)
    {
      String body = "{ \"board_id\": \"" + Board_UID + "\", \"button_no\": \"3\" }";
      char copy[255];
      body.toCharArray(copy, 255);
      socket.emit("push_button", copy);
      if (STATE_SERVO_3 == 1)
      {
        servoDown(3);
      }
      else
      {
        servoUp(3);
      }
    }
    delay(100);
  }
  latest_state_btn_3 = digitalRead(SW_3);
}

void handleSensor()
{
  long t, detect_cm;
  if (TRIGGERED_RANGE != 0)
  {
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    // v = 340.29 (m/s) -> 29 centimeter/microseconds
    // t = duration / 2 (ms)
    t = pulseIn(echoPin, HIGH);
    detect_cm = t / 29 / 2;
    Serial.println(detect_cm);
    if (detect_cm <= TRIGGERED_RANGE && detect_cm != 0)
    {
      Serial.println("[SWiTH] Sensor detected!");
      Serial.println(detect_cm);
      String body = "{ \"board_id\": \"" + Board_UID + "\" }";
      char copy[255];
      body.toCharArray(copy, 255);
      socket.emit("sensor_detect", copy);
    }
  }
}

String readString;

void socketConnected(const char *payload, size_t length)
{
  Serial.println("[SWiTH] Socket.IO connected!");
  String body = "{ \"board_id\": \"" + Board_UID + "\", \"ip_address\": \"" + WiFi.localIP().toString() + "\" }";
  char copy[255];
  body.toCharArray(copy, 255);
  socket.emit("joins", copy);
  SOCKET_IO_CONNECTED = 1;
}

void handleUpdateServoState(const char *json, size_t length)
{
  const size_t capacity = JSON_OBJECT_SIZE(6) + 70;
  DynamicJsonDocument result(capacity);
  DeserializationError error = deserializeJson(result, json);
  if (error)
  {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
  }
  int servo_1 = result["servo_1"];
  int servo_2 = result["servo_2"];
  int servo_3 = result["servo_3"];
  int sensor_range = result["sensor_range"];
  String sensor_mode = result["sensor_mode"];
  int servo_number = result["sensor_no"];
  // update global variables
  STATE_SERVO_1 = servo_1;
  STATE_SERVO_2 = servo_2;
  STATE_SERVO_3 = servo_3;
  TRIGGERED_RANGE = sensor_range;
}

void handleServoControl(const char *json, size_t length)
{
  const size_t capacity = JSON_OBJECT_SIZE(2) + 20;
  DynamicJsonDocument result(capacity);
  DeserializationError error = deserializeJson(result, json);

  if (error)
  {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
  }

  int servo_number = result["servo"];        // 1,2,3
  int servo_direction = result["direction"]; // 1 = up , 2 = down

  if (servo_direction == 1)
  {
    servoUp(servo_number);
  }
  else
  {
    servoDown(servo_number);
  }
}

void servoUp(int number)
{
  const int DEGREE = INITIAL_DEGREE + 68;

  if (number == 1)
  {
    servo1.write(INITIAL_DEGREE - 65);
  }
  else if (number == 2)
  {
    servo2.write(DEGREE);
  }
  else
  {
    servo3.write(DEGREE + 5);
  }
  delay(500);
  servoReset();
  Serial.println("Servo Up");
}

void servoDown(int number)
{
  Serial.println("Servo Down");
  const int DEGREE_DOWN = INITIAL_DEGREE - 65;
  if (number == 1)
  {
    servo1.write(INITIAL_DEGREE + 68);
  }
  else if (number == 2)
  {
    servo2.write(DEGREE_DOWN);
  }
  else
  {
    servo3.write(DEGREE_DOWN);
  }
  delay(500);
  servoReset();
}

void servoReset()
{
  servo1.write(INITIAL_DEGREE);
  servo2.write(INITIAL_DEGREE + 2);
  servo3.write(INITIAL_DEGREE);
}

void on_disconnect(const char *payload, size_t length)
{
  Serial.println("[SWiTH] Socket.IO disconnceted.");
}
